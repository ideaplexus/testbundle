<?php

namespace IPC\TestBundle;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait SymfonyValidatorConstraintTrait
{
    /**
     * Assert that a violation exists in a ConstraintViolationList.
     *
     * @param string                           $messageTemplate
     * @param string                           $propertyPath
     * @param ConstraintViolationListInterface $violationList
     * @param string                           $message
     *
     * @return void
     */
    protected function assertViolationExists(
        string $messageTemplate,
        string $propertyPath,
        ConstraintViolationListInterface $violationList,
        string $message = ''
    ): void {
        TestCase::assertThat(
            $messageTemplate,
            new SymfonyValidatorConstraint($violationList, $propertyPath),
            $message
        );
    }
}
