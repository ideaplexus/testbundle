<?php

namespace IPC\TestBundle;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class AbstractControllerTestCase extends AbstractSymfonyTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @inheritdoc
     * Set up a default client
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createClient([
            'TEST_AUTH_USER' => getenv('TEST_AUTH_USER') ?: 'test',
            'TEST_AUTH_PASS' => getenv('TEST_AUTH_PASS') ?: 'test',
        ]);
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route         The name of the route
     * @param array  $parameters    An array of parameters
     * @param int    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    protected function generateUrl($route, $parameters = [], $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * Assert for \DOMElements (and their count) based on given selector.
     *
     * @param Crawler $crawler   The Crawler
     * @param array   $selectors The selectors to check for \DOMElements
     */
    protected function assertDOMElements($crawler, $selectors)
    {
        foreach ($selectors as $key => $value) {
            $count    = 1;
            $selector = $value;

            if (\is_string($key) && \is_int($value)) {
                $count    = $value;
                $selector = $key;
            }

            $this->assertCount(
                $count,
                $crawler->filter($selector),
                \sprintf('Element "%s" not matching the expected count.', $selector)
            );
        }
    }

    /**
     * Creates a Client.
     *
     * WARNING:
     * per default this client runs in insulate-mode
     * change this behavior not here, but before you use this client
     *
     * @param array $server  An array of server parameters
     * @param array $services
     *
     * @return Client A Client instance
     */
    protected function createClient(array $server = [], array $services = []): Client
    {
        $client = static::$kernel->getContainer()->get('test.client');
        $client->setServerParameters($server);
        foreach ($services as $key => $service) {
            $client->getContainer()->set($key, $service);
        }

        return $client;
    }
}
