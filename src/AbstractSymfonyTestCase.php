<?php

namespace IPC\TestBundle;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractSymfonyTestCase extends KernelTestCase
{
    use SymfonyValidatorConstraintTrait;

    /**
     * @var RegistryInterface
     */
    protected $registry;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * Boots kernel and set up container, doctrine registry and symfony validator
     */
    protected function setUp(): void
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
        $this->registry  = $this->container->get('doctrine');
        $this->validator = $this->container->get('validator');
    }
}
