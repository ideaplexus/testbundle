<?php

namespace IPC\TestBundle;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class TestEntityHelper
 */
class TestEntityHelper
{
    /**
     * @var RegistryInterface
     */
    protected $registry;

    /**
     * @var array
     */
    protected $entities = [];

    /**
     * @var array
     */
    protected $classMetaData = [];

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @return RegistryInterface
     */
    protected function getRegistry(): RegistryInterface
    {
        return $this->registry;
    }

    /**
     * Get a an entity by class name and identifier from object manager.
     *
     * @param string $className  The class name
     * @param mixed  $identifier The identifier(s)
     *
     * @return mixed Returns entity
     *
     * @throws EntityNotFoundException
     */
    public function getEntity(string $className, $identifier): object
    {
        if (\is_array($identifier) && \count($identifier) === 1) {
            $identifier = \reset($identifier);
        }

        try {
            $entity = $this
                ->getManagerForClass($className)
                ->find($className, $identifier);
            if (!$entity) {
                throw new EntityNotFoundException();
            }

            return $entity;
        } catch (\Exception $e) {
            throw EntityNotFoundException::fromClassNameAndIdentifier($className, (array) $identifier);
        }
    }

    /**
     * Remove an entity.
     *
     * @param mixed $entity  The entity
     * @param bool $deferred True if remove should be deferred
     *
     * @return $this
     *
     * @throws ORMInvalidArgumentException
     * @throws EntityNotFoundException
     */
    public function remove(object $entity, bool $deferred = false): self
    {
        if ($deferred) {
            $className = \get_class($entity);
            \array_unshift($this->entities, [$className, $this->getIdentifier($entity)]);
        } else {
            $this->removeByClassAndIdentifier(\get_class($entity), $this->getIdentifier($entity));
        }

        return $this;
    }

    /**
     * Remove all deferred entities.
     *
     * @return $this
     *
     * @throws \RuntimeException
     */
    public function removeDeferred(): self
    {
        $this->getRegistry()->resetManager();
        $removedEntities = -1;
        while ($removedEntities !== 0) {
            $removedEntities = 0;
            foreach ($this->entities as $key => $value) {
                try {
                    $this->removeByClassAndIdentifier($value[0], $value[1]);
                    unset($this->entities[$key]);
                    $removedEntities++;
                } catch (\Exception $e) {
                    // ignore exception since a foreign key constraint may exists, but have to reset manager
                    $this->getRegistry()->resetManager();
                }
            }
        }
        if (!empty($this->entities)) {
            throw new \RuntimeException('Could not remove all deferred entities, maybe an deadlock was detected!');
        }

        return $this;
    }

    /**
     * Remove an entity direct by class name and identifier
     *
     * @param string $className  The class name
     * @param mixed  $identifier The identifier(s)
     *
     * @return $this
     *
     * @throws EntityNotFoundException
     * @throws ORMInvalidArgumentException
     */
    protected function removeByClassAndIdentifier(string $className, $identifier): self
    {
        $entity        = $this->getEntity($className, $identifier);
        $objectManager = $this->getManagerForClass($className);

        $objectManager->remove($entity);
        $objectManager->flush();

        return $this;
    }
    /**
     * Get identifier (values) from an entity
     *
     * @param object $entity The entity
     *
     * @return array
     *
     * @throws ORMInvalidArgumentException
     */
    protected function getIdentifier(object $entity): array
    {
        $values      = [];
        $className   = \get_class($entity);
        $identifiers = $this->getClassMetadata($className)->getIdentifier();
        $reflection  = new \ReflectionClass($entity);

        foreach ($identifiers as $identifier) {
            $property = $reflection->getProperty($identifier);
            $property->setAccessible(true);
            $values[] = $property->getValue($entity);
        }

        return $values;
    }

    /**
     * Get class metadata from doctrine.
     *
     * @param string $className The class name
     *
     * @return ClassMetadata
     *
     * @throws ORMInvalidArgumentException
     */
    protected function getClassMetadata(string $className): ClassMetadata
    {
        if (!isset($this->classMetaData[$className])) {
            $this->classMetaData[$className] = $this->getManagerForClass($className)->getClassMetadata($className);
        }

        return $this->classMetaData[$className];
    }

    /**
     * @param $className
     *
     * @return ObjectManager
     *
     * @throws ORMInvalidArgumentException
     */
    protected function getManagerForClass(string $className) : ObjectManager
    {
        $manager = $this->getRegistry()->getManagerForClass($className);

        if (!$manager) {
            throw ORMInvalidArgumentException::entityNotManaged(new class($className) {
                protected $className;
                public function __construct(string $className) { $this->className = $className; }
                public function __toString() { return $this->className; }
            });
        }

        return $manager;
    }
}
