<?php

namespace IPC\Tests\TestBundle;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\ORMInvalidArgumentException;
use IPC\TestBundle\TestEntityHelper;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @coversDefaultClass IPC\TestBundle\TestEntityHelper
 */
class TestEntityHelperTest extends TestCase
{
    /**
     * @var TestEntityHelper|MockObject
     */
    protected $helper;

    /**
     * @return void
     *
     * @covers ::__construct
     * @covers ::getRegistry
     */
    public function testConstructAndGetRegistry(): void
    {
        $registry   = $this->createMock(RegistryInterface::class);
        $helper     = new TestEntityHelper($registry);
        $reflection = new \ReflectionMethod($helper, 'getRegistry');
        $reflection->setAccessible(true);
        $this->assertEquals($registry, $reflection->invoke($helper));
    }

    /**
     * @return void
     *
     * @covers ::getEntity
     */
    public function testGetEntityInvalidClass(): void
    {
        $exception = $this->createMock(ORMInvalidArgumentException::class);
        $helper    = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getManagerForClass'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getManagerForClass')
            ->willThrowException($exception);

        $this->expectException(EntityNotFoundException::class);

        $helper->getEntity('some_class', 'some_identifier');
    }

    /**
     * @return void
     *
     * @covers ::getEntity
     */
    public function testGetEntityNotFound(): void
    {
        $manager = $this->createMock(ObjectManager::class);
        $helper  = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getManagerForClass'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getManagerForClass')
            ->willReturn($manager);

        $manager
            ->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $this->expectException(EntityNotFoundException::class);

        $helper->getEntity('some_class', 'some_identifier');
    }

    /**
     * @return void
     *
     * @covers ::getEntity
     */
    public function testGetEntity(): void
    {
        $entity  = new class {};
        $manager = $this->createMock(ObjectManager::class);
        $helper  = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getManagerForClass'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getManagerForClass')
            ->willReturn($manager);

        $manager
            ->expects($this->once())
            ->method('find')
            ->willReturn($entity);

        $this->assertEquals($entity, $helper->getEntity('some_class', 'some_identifier'));
    }

    /**
     * @return void
     *
     * @covers ::getEntity
     */
    public function testGetEntityIdentifierConverted(): void
    {
        $className  = 'some_class';
        $identifier = 'some_identifier';
        $entity     = new class {};
        $manager    = $this->createMock(ObjectManager::class);
        $helper     = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getManagerForClass'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getManagerForClass')
            ->willReturn($manager);

        $manager
            ->expects($this->once())
            ->method('find')
            ->with($className, $identifier)
            ->willReturn($entity);

        $helper->getEntity($className, [$identifier]);
    }

    /**
     * @return void
     *
     * @covers ::remove
     */
    public function testRemoveWithDeferredFalse(): void
    {
        $entity = new class {};
        $helper = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getIdentifier', 'removeByClassAndIdentifier'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getIdentifier')
            ->with($entity);

        $helper
            ->expects($this->once())
            ->method('removeByClassAndIdentifier');

        $this->assertEquals($helper, $helper->remove($entity));
    }

    /**
     * @return void
     *
     * @covers ::remove
     */
    public function testRemoveWithDeferredTrue(): void
    {
        $entity = new class {};
        $helper = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getIdentifier', 'removeByClassAndIdentifier'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getIdentifier')
            ->with($entity);

        $helper
            ->expects($this->never())
            ->method('removeByClassAndIdentifier');

        $this->assertEquals($helper, $helper->remove($entity, true));
    }

    /**
     * @return void
     *
     * @covers ::removeDeferred
     */
    public function testRemoveDeferredNoEntities(): void
    {
        $registry = $this->createMock(RegistryInterface::class);
        $registry
            ->expects($this->once())
            ->method('resetManager');

        $helper = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getRegistry'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getRegistry')
            ->willReturn($registry);

        $this->assertEquals($helper, $helper->removeDeferred());
    }

    /**
     * @return void
     *
     * @covers ::removeDeferred
     */
    public function testRemoveDeferredRemoveEntityFailed(): void
    {
        $entity     = new class {};
        $identifier = ['some_identifier'];
        $exception  = $this->createMock(EntityNotFoundException::class);
        $registry   = $this->createMock(RegistryInterface::class);
        $helper     = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getIdentifier', 'removeByClassAndIdentifier', 'getRegistry'])
            ->getMock();

        $registry
            ->expects($this->exactly(2))
            ->method('resetManager');

        $helper
            ->expects($this->exactly(2))
            ->method('getRegistry')
            ->willReturn($registry);

        $helper
            ->expects($this->once())
            ->method('removeByClassAndIdentifier')
            ->willThrowException($exception);

        $reflection = new \ReflectionProperty($helper, 'entities');
        $reflection->setAccessible(true);
        $reflection->setValue($helper, [[\get_class($entity), $identifier]]);

        $this->expectException(\RuntimeException::class);

        $helper->removeDeferred();
    }

    /**
     * @return void
     *
     * @covers ::removeDeferred
     */
    public function testRemoveDeferredRemoveEntity(): void
    {
        $entity     = new class {};
        $identifier = ['some_identifier'];
        $registry   = $this->createMock(RegistryInterface::class);
        $helper     = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getIdentifier', 'removeByClassAndIdentifier', 'getRegistry'])
            ->getMock();

        $registry
            ->expects($this->once())
            ->method('resetManager');

        $helper
            ->expects($this->once())
            ->method('getRegistry')
            ->willReturn($registry);

        $helper
            ->expects($this->once())
            ->method('removeByClassAndIdentifier');

        $reflection = new \ReflectionProperty($helper, 'entities');
        $reflection->setAccessible(true);
        $reflection->setValue($helper, [[\get_class($entity), $identifier]]);

        $helper->removeDeferred();
    }

    /**
     * @return void
     *
     * @covers ::removeByClassAndIdentifier
     */
    public function testRemoveByClassAndIdentifier(): void
    {
        $entity     = new class {};
        $className  = 'some_class';
        $identifier = ['some_identifier'];
        $helper     = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntity', 'getManagerForClass'])
            ->getMock();

        $objectManager = $this->createMock(ObjectManager::class);

        $helper
            ->expects($this->once())
            ->method('getEntity')
            ->with($className, $identifier)
            ->willReturn($entity);

        $helper
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn($objectManager);

        $objectManager
            ->expects($this->once())
            ->method('remove')
            ->with($entity);

        $objectManager
            ->expects($this->once())
            ->method('flush');

        $reflection = new \ReflectionMethod($helper, 'removeByClassAndIdentifier');
        $reflection->setAccessible(true);

        $this->assertEquals($helper, $reflection->invokeArgs($helper, [$className, $identifier]));
    }

    /**
     * @return void
     *
     * @covers ::getIdentifier
     */
    public function testGetIdentifier(): void
    {
        $entity     = new class { protected $some_identifier = 'some_value'; };
        $identifier = ['some_identifier'];
        $metadata   = $this->createMock(ClassMetadata::class);
        $helper     = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getClassMetadata'])
            ->getMock();

        $metadata
            ->expects($this->once())
            ->method('getIdentifier')
            ->willReturn($identifier);

        $helper
            ->expects($this->once())
            ->method('getClassMetadata')
            ->willReturn($metadata);

        $reflection = new \ReflectionMethod($helper, 'getIdentifier');
        $reflection->setAccessible(true);

        $this->assertEquals(['some_value'], $reflection->invoke($helper, $entity));
    }

    /**
     * @return void
     *
     * @covers ::getClassMetadata
     */
    public function testGetClassMetadata(): void
    {
        $className = 'some_class';
        $manager   = $this->createMock(ObjectManager::class);
        $metadata  = $this->createMock(ClassMetadata::class);
        $helper    = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getManagerForClass'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn($manager);

        $manager
            ->expects($this->once())
            ->method('getClassMetadata')
            ->with($className)
            ->willReturn($metadata);

        $reflection = new \ReflectionMethod($helper, 'getClassMetadata');
        $reflection->setAccessible(true);

        $this->assertEquals($metadata, $reflection->invoke($helper, $className));

        // second time the cache should be used
        $this->assertEquals($metadata, $reflection->invoke($helper, $className));
    }

    /**
     * @return void
     *
     * @covers ::getManagerForClass
     */
    public function testGetManagerForClassORMInvalidArgumentException(): void
    {
        $className = 'some_class';
        $registry  = $this->createMock(RegistryInterface::class);

        $helper    = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getRegistry'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getRegistry')
            ->willReturn($registry);

        $registry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn(null);

        $this->expectException(ORMInvalidArgumentException::class);

        $reflection = new \ReflectionMethod($helper, 'getManagerForClass');
        $reflection->setAccessible(true);
        $reflection->invoke($helper, $className);
    }

    /**
     * @return void
     *
     * @covers ::getManagerForClass
     */
    public function testGetManagerForClass(): void
    {
        $className = 'some_class';
        $registry  = $this->createMock(RegistryInterface::class);
        $manager   = $this->createMock(ObjectManager::class);
        $helper    = $this
            ->getMockBuilder(TestEntityHelper::class)
            ->disableOriginalConstructor()
            ->setMethods(['getRegistry'])
            ->getMock();

        $helper
            ->expects($this->once())
            ->method('getRegistry')
            ->willReturn($registry);

        $registry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn($manager);

        $reflection = new \ReflectionMethod($helper, 'getManagerForClass');
        $reflection->setAccessible(true);

        $this->assertEquals($manager, $reflection->invoke($helper, $className));
    }
}
